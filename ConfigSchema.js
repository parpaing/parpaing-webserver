const joi = require('@hapi/joi');

const ConfigSchema = joi.object().keys({
    hostname: joi.string().hostname().allow(null).empty([null])
        .default('::'),
    port: joi.number().min(0).max(65535).allow(null)
        .empty([null])
        .default(80)
}).unknown().required();

module.exports = ConfigSchema;

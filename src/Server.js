const http = require('http');
const path = require('path');

const bodyParser = require('body-parser');
const connectFlash = require('connect-flash');
const express = require('express');
const expressSession = require('express-session');
const morgan = require('morgan');
const passport = require('passport');
const serveFavicon = require('serve-favicon');

class Server {
    constructor(WS) {
        this.WS = WS;

        // Express App Object
        this.app = express();
        this.Router = express.Router();

        // View System
        this.app.set('view engine', this.WS.config.app.viewEngine);
        this.viewsFolders = [];

        // Request Logger Middleware
        this.app.use(morgan('short'));

        // Body Parsing Middleware
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: true }));

        // Flash Middleware
        this.app.use(connectFlash());

        // Session Middleware
        this.app.use(expressSession({
            secret: this.WS.config.app.session.secret,
            resave: false,
            saveUninitialized: false
        }));

        // Passport Middleware
        this.app.use(passport.initialize());
        this.app.use(passport.session());

        // Locals Data
        this.app.locals = {};
        if (typeof this.WS.config.app.locals === 'object') {
            this.app.locals = Object.assign(this.app.locals, this.WS.config.app.locals);
        }

        // HTTP Server
        this.server = http.createServer(this.app);
    }

    setupFavicon(faviconPath) {
        this.app.use(serveFavicon(faviconPath));
    }

    setupBaseDir(root) {
        this.app.locals.basedir = root;
    }

    addPublicFolder(root) {
        this.app.use('/img', express.static(path.join(root, 'img')));
        this.app.use('/js', express.static(path.join(root, 'js')));
        this.app.use('/css', express.static(path.join(root, 'css')));
    }

    addViewsFolder(root) {
        this.viewsFolders.push(root);

        this.app.set('views', this.viewsFolders);
    }

    start() {
        this.app.use(this.Router);

        try {
            this.server.listen(this.WS.config.port, this.WS.config.hostname, () => {
                this.WS.logger.info(`[WebServer] Server started on ${this.WS.config.hostname}:${this.WS.config.port}`);
            });
        } catch (e) {
            throw new Error(e);
        }
    }

    stop() {
        this.WS.logger.info('[WebServer] Stopping server...');
        this.server.close(() => {
            this.WS.logger.info('[WebServer] Server stopped');
        });
    }
}

module.exports = Server;

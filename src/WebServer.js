const path = require('path');
const fs = require('fs');

const ParpaingMod = require('@parpaing/parpaing').Mod;

const Server = require('./Server');

class WebServer extends ParpaingMod {
    setup() {
        this.Server = new Server(this);

        if (fs.existsSync(path.join(this.root, 'favicon.ico'))
                && fs.statSync(path.join(this.root, 'favicon.ico')).isFile()) {
            this.Server.setupFavicon(path.join(this.root, 'favicon.ico'));
        }

        if (fs.existsSync(path.join(this.root, 'public'))
                && fs.statSync(path.join(this.root, 'public')).isDirectory()) {
            this.Server.addPublicFolder(path.join(this.root, 'public'));
        }

        if (fs.existsSync(path.join(this.root, 'views'))
                && fs.statSync(path.join(this.root, 'views')).isDirectory()) {
            this.Server.setupBaseDir(path.join(this.root, 'views'));
            this.Server.addViewsFolder(path.join(this.root, 'views'));
        }

        if (fs.existsSync(path.join(this.root, 'routes'))
                && fs.statSync(path.join(this.root, 'routes')).isDirectory()) {
            this.addRoutesFolder(path.join(this.root, 'routes'));
        }
    }

    getRouter() {
        return this.Server.Router;
    }

    addPublicFolder(root) {
        this.Server.addPublicFolder(root);
    }

    addViewsFolder(root) {
        this.Server.addViewsFolder(root);
    }

    addRouter(routerPath, router) {
        this.Server.app.use(routerPath, router);
    }

    addRoutesFolder(root, ...args) {
        try {
            fs.readdirSync(root).forEach((filename) => {
                require(path.join(root, filename))(this, ...args);
            });
        } catch (err) {
            this.logger.debug('[WebServer] Can\'t load routes from folder %s. Error: %s', root, err);
        }
    }

    start() {
        this.Server.start();
    }

    stop() {
        this.Server.stop();
    }
}

module.exports = WebServer;

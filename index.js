const ConfigSchema = require('./ConfigSchema');
const WebServer = require('./src/WebServer');

module.exports = {
    Mod: WebServer,
    ConfigSchema
};
